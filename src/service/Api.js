import axios from 'axios';

const api = axios.create({ baseURL: "https://iduff-os-salientes.herokuapp.com"});

export default api;