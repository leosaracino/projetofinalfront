import React from 'react';
import Botao from '../../common/Botao/Botao';

import Image from '../../assets/notFound.gif'

function NotFound() {
    return (
        <div className="wrapper">
            <div className="alert-container">
                <h1>UFF</h1>
                <h2>Erro 404</h2>
                <img src={Image} alt="JohnTravolta_confused"/>
                <Botao fillColor="blue" label="Voltar"/>
            </div>
        </div>
    )
}

export default NotFound;
