import React , { useState, useEffect, useContext } from 'react';
import { Context } from '../../Context/AuthContext';
import PageHeader from '../../common/PageHeader/PageHeader';
import SideBar from '../../common/SideBar/SideBar';
import Footer from '../../common/Footer/Footer'

function Resume() {
    const { config, api } = useContext( Context );

    const [courses, setCourses] = useState([]);

    const [selectValue, setSelectValue] = useState("");

    useEffect(() => {
        api.get('/courses', config)
        .then( (response) => {
            setCourses(response.data)
        })
    }, [api])

    function updatesTable(courses, selectedCourseId) {
        courses.forEach((i) => {
            if (i["id"] === parseInt(selectedCourseId)) {
                return (
                    i.course_disciplines.map((d) => {
                        <tr>
                            <tr>
                                {d.name}
                            </tr>
                            <tr>
                                {d.order}
                            </tr>
                            <tr>
                                {d.workload}
                            </tr>
                        </tr>
                    })
                )
            }
        })
    }

    return (
        <section>
        <SideBar />
        <div className="page-wrapper">
            <div className="page-container">
                <PageHeader title="Visualizar Currículos"/>
                <p>Visualize aqui o currículo de qualquer um dos cursos da UFF.</p>
                <select className="input-style" id="courses-select" value={selectValue} onChange={(e) => setSelectValue(e.target.value)}>
                    <option value="" defaultValue>Selecione um curso</option>
                    {courses.map(i => {
                    return (
                        <option key={i.id} value={i.id}>{i.name}</option>
                    )
                    })}
                </select>

                <table className="table three-columns">
                    <tr>
                        <th>
                            Nome
                        </th>
                        <th>
                            Período
                        </th>
                        <th>
                            CHT
                        </th>
                    </tr>
                    {
                        updatesTable(courses, selectValue)
                    }
                </table>
            </div>
            <Footer />
        </div>
        </section>
    )
}

export default Resume;
