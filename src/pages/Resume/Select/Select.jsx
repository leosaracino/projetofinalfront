import React , { useContext, useState, useEffect } from 'react';
import { Context } from '../../../Context/AuthContext';
import './Select.css';
// import data from '../../../service/cursostest';

function Select(label, options) {
    const { api , config } = useContext(Context);
    const [courses, setCourses] = useState([]);
    
    useEffect(() => {
        api.get('/courses', config)
        .then((response) => {  
            setCourses(response.data)
        })
        .catch((err) => {
            console.warn(err)
        })  
    }, [api])

    return (
        <select className="select-style">
            <option value="" defaultValue></option>
            { courses != [] ? courses.map(i => {
                return (
                    <option key={i.id} value={i.id}>{i.name}</option>
                )
            }) : <p>.</p>}
        </select>
    )
}

export default Select;
