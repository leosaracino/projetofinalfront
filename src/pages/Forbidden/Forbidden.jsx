import React from 'react';

import Botao from '../../common/Botao/Botao';
import Image from '../../assets/Forbidden.jpg'

function Forbidden() {
    return (
        <div className="wrapper">
            <div className="alert-container">
                <h1>UFF</h1>
                <h2>Erro 401</h2>
                <img src={Image} alt="BugsBunny_denial"/>
                <Botao fillColor="blue" label="Voltar"/>
            </div>
        </div>
    )
}

export default Forbidden;
