import React , { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Context } from '../../../Context/AuthContext';
import { postStudentReq } from '../../../Context/Hooks/postStudentReq';

import SideBar from '../../../common/SideBar/SideBar';
import PageHeader from '../../../common/PageHeader/PageHeader';
import SelectBox from '../../../common/SelectBox/SelectBox';
import Footer from '../../../common/Footer/Footer';
import Botao from '../../../common/Botao/Botao';
import Input from '../../../common/Input/Input';

function StudentRegistration() {
    const [name, setName] = useState("");
    const [state, setState] = useState("");
    const [birth, setBirth] = useState("");
    const [rg, setRg] = useState("");
    const [cpf, setCpf] = useState("");
    const [email, setEmail] = useState("");
    const [nacionality, setNacionality] = useState("");

    const history = useHistory();
    const { setNavigation } = useContext( Context );

    const { api, IBGE , config} = useContext( Context );
    const [brazilStates, setBrazilStates] = useState([]);

    useEffect(() => {
        IBGE.get('/estados')
        .then((response) => {  
            let states = [];
            response.data.map(i => {
                states.push(i.nome);
                return null;
            })
            states.sort();
            setBrazilStates(states);
        })
        .catch((err) => {
            console.warn(err);
        })  
    }, [IBGE])

    const [courseInfo, setcourseInfo] = useState([]);

    useEffect(() => {
        api.get('/show_course_coordC', config)
        .then( (response) => {
            setcourseInfo(response.data)
        })
    }, [api])
    
    return (
        <section>
        <SideBar />
        <div className="page-wrapper">
            <div className="page-container">
                <PageHeader title="Cadastro de alunos"/>
                <h3>{`Crie usuários para alunos do curso ${courseInfo.name}`}</h3>               
                <div className="create-form-container">
                	<Input type="text" placeholder="John Doe" label="Nome:" actualValue={name} changeValue={setName} id="coord-name"/>
                    <Input type="text" placeholder="joaozinhogost0s40@id.uff.br" label="E-mail: " actualValue={email} changeValue={setEmail} />
                    <Input type="number" placeholder="ddmmaaaa" label="Nascimento:" actualValue={birth} changeValue={setBirth} />
                    <Input type="text" placeholder="Brasileiro(a)" label="Nacionalidade: " actualValue={nacionality} changeValue={setNacionality} />
                    <SelectBox label="Selecione um Estado" listedOptions={brazilStates} actualValue={state} changeValue={setState}/>
                    <Input type="number" placeholder="00.000.000-0" label="RG" actualValue={rg} changeValue={setRg} />
                    <Input type="number" placeholder="000.000.000-00" label="CPF:" actualValue={cpf} changeValue={setCpf} />
                </div>
               
                <div className="action-buttons">
                    <Botao fillColor="red" label="Cancelar" clickFunction={() => setNavigation(history, 'my-dashboard')}/>
                    <Botao fillColor="blue" label="Salvar" clickFunction={() => postStudentReq(config, name, birth, rg, cpf, state, email, nacionality, setName, setBirth, setRg, setCpf, setState, setEmail, setNacionality)}/>
                </div>
            </div>
            <Footer />
        </div>
        </section>    
    )
}

export default StudentRegistration;
