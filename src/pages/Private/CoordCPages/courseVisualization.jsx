import React , { useState, useEffect, useContext } from 'react';
import { Context } from '../../../Context/AuthContext';

import SideBar from '../../../common/SideBar/SideBar';
import PageHeader from '../../../common/PageHeader/PageHeader';
import Footer from '../../../common/Footer/Footer';
function CourseVisualization() {
    const { config, api } = useContext( Context );

    const [courseInfo, setcourseInfo] = useState([]);

    useEffect(() => {
        api.get('/show_course_coordC', config)
        .then( (response) => {
            setcourseInfo(response.data)
        })
    }, [api])
    
    return (
        <section>
            <SideBar />
            <div className="page-wrapper">
                <div className="page-container">
                    <PageHeader title={courseInfo.name} />
                    <div className="info-container">
                    <h3>Veja informações sobre o seu curso</h3>
                    	<p>{`Curso: ${courseInfo.name}`}</p>
                    	<p>{`Campus Pertencente: ${courseInfo.campus}`}</p>
                    	<p>{`Área de Conhecimento: ${courseInfo.knowledge_area}`}</p>          
                    	<p>{`Código do Curso: ${courseInfo.code}`}</p>
                    	<p>{`Coordenadora responsável: ${courseInfo.coord_name}`}</p>
                    	<p>{`Estudantes matriculados: ${courseInfo.number_students}`}</p>
                    </div>
                </div>
                <Footer />
            </div>
        </section>    
    )
}

export default CourseVisualization;