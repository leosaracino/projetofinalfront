import React , { useState, useEffect, useContext } from 'react';
import { Context } from '../../../Context/AuthContext';

import SideBar from '../../../common/SideBar/SideBar';
import PageHeader from '../../../common/PageHeader/PageHeader';
import Footer from '../../../common/Footer/Footer';
function StudentsVisualization() {
    const { config, api } = useContext( Context );

    const [courseInfo, setcourseInfo] = useState([]);

    useEffect(() => {
        api.get('/show_course_coordC', config)
        .then( (response) => {
            setcourseInfo(response.data)
        })
    }, [])
    
    return (
        <section>
            <SideBar />
            <div className="page-wrapper">
                <div className="page-container">
                    <PageHeader title={`Alunos de ${courseInfo.name}`} />
                    <h3>Informações dos alunos matriculados</h3>

                    <tbody className="table two-columns">
                        <tr>
                            <th>Nome</th>
                            <th>Id</th>
                        </tr>

                        { courseInfo.students !== undefined ? courseInfo.students.map(i => {
                            return (
                                <tr id={i.key}>
                                    <td>
                                        {i.name_student}
                                    </td>
                                    <td>
                                        {i.id}
                                    </td>
                                </tr>
                            )
                        }) : <h3>Carregando...</h3>}    
                    </tbody>

                    	{/* <p>{`Curso: ${courseInfo.name}`}</p>
                    	<p>{`Campus Pertencente: ${courseInfo.campus}`}</p>
                    	<p>{`Área de Conhecimento: ${courseInfo.knowledge_area}`}</p>          
                    	<p>{`Código do Curso: ${courseInfo.code}`}</p>
                    	<p>{`Coordenadora responsável: ${courseInfo.coord_name}`}</p>
                    	<p>{`Estudantes matriculados: ${courseInfo.number_students}`}</p> */}
                    </div>
                </div>
                <Footer />
        </section>    
    )
}

export default StudentsVisualization;