import React , { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Context } from '../../../Context/AuthContext';
import { postDeptReq } from '../../../Context/Hooks/postDeptReq';

import SideBar from '../../../common/SideBar/SideBar';
import PageHeader from '../../../common/PageHeader/PageHeader';
import SelectBox from '../../../common/SelectBox/SelectBox';
import Footer from '../../../common/Footer/Footer';
import Botao from '../../../common/Botao/Botao';
import Input from '../../../common/Input/Input';

function DepartmentRegistration() {
    const [knowledgeArea, setKnowledgeArea] = useState("");
    const [userId, setUserId] = useState("");
    const [campus, setCampus] = useState("");
    const [courseName, setCourseName] = useState("");
    const [courseCode, setCourseCode] = useState("");

    const [coordinators, setCoordinators] = useState([]);

    const history = useHistory();

    const { setNavigation, api, config } = useContext( Context );

    const knowledgeAreas = [
    "Ciências Exatas e da Terra",
    "Ciências Biológicas",
    "Engenharias",
    "Ciências da Saúde",
    "Ciências Agrárias",
    "Linguística, Letras e Artes",
    "Ciências Sociais Aplicadas",
    "Ciências Humanas"]

    const campuses = [
    "Praia-Vermelha",
    "Gragoatá",
    "Valonguinho",
    "Medicina (HUAP)",
    "Direito",
    "IACS",
    "Farmácia"
    ]

    useEffect(() => {
        let coords = [];
        api.get( '/get_coordD' , config )
        .then((response) => {
            response.data.map(i => {
                coords.push(i.id);
            })
            setCoordinators(coords);
        })
         
    }, [])
    
    return (
        <section>
        <SideBar />
        <div className="page-wrapper">
            <div className="page-container">
                <PageHeader title="Cadastro de departamentos"/>
                <h3>Crie novos departamentos para sua universidade</h3>
                <div className="create-form-container">
                	<SelectBox label="Área de conhecimento" listedOptions={knowledgeAreas} actualValue={knowledgeArea} changeValue={setKnowledgeArea}/>
                    <SelectBox label="Campus/Sede" listedOptions={campuses} actualValue={campus} changeValue={setCampus}/>
                    <div className="course-name">
                        <Input type="text" placeholder="157" label="Código do departamento: " actualValue={courseCode} changeValue={setCourseCode}/>
                        <Input type="text" placeholder="Embriologia ufológica III" label="Nome do departamento: " actualValue={courseName} changeValue={setCourseName}/>
                    </div>
                    <SelectBox label="Id do Coordenador" listedOptions={coordinators} actualValue={userId} changeValue={setUserId}/>
                </div>   
                <div className="action-buttons">
                    <Botao fillColor="red" label="Cancelar" clickFunction={() => setNavigation(history, 'my-dashboard')}/>
                    <Botao fillColor="blue" label="Salvar" clickFunction={() => postDeptReq(config, knowledgeArea, campus, courseCode, courseName, userId, setKnowledgeArea, setCampus, setCourseCode, setCourseName, setUserId)}/>
                </div>
            </div>
            <Footer />
        </div>
        </section>    
    )
}

export default DepartmentRegistration;