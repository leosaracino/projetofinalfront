import React , { useState, useContext } from 'react'

import { Context } from '../../../Context/AuthContext';

import SideBar from '../../../common/SideBar/SideBar';
import PageHeader from '../../../common/PageHeader/PageHeader';
import SelectBox from '../../../common/SelectBox/SelectBox';
import Footer from '../../../common/Footer/Footer';
import Botao from '../../../common/Botao/Botao';

import { postSeasonReq } from '../../../Context/Hooks/postSeasonReq'; 

function SeasonEdit() {
    const { config } = useContext(Context);
    const [seasonYear, setSeasonYear] = useState("");
    const [season, setSeason] = useState("")
    const [statusCode, setStatusCode] = useState("")

    const years = ["2020", "2021","2023","2024"];
    const seasons = [".1", ".2"];
    const seasonStatus = ["0", "1", "2", "3"];

    return (
        <section>
        <SideBar />
        <div className="page-wrapper">
            <div className="page-container">
                <PageHeader title="Planejamento de Período"/>
                <h3>Edite ou planeje as informações do período desejado</h3>
                
                <div className="create-form-container">
                	<p>Ano do período</p><SelectBox label="Selecione um ano" listedOptions={years} actualValue={seasonYear} changeValue={setSeasonYear}/>
                	<p>Semestre do período</p><SelectBox label="Selecione um semestre" listedOptions={seasons} actualValue={season} changeValue={setSeason}/>
                	<p>Status:</p>
                    <SelectBox label="Selecione um status" listedOptions={seasonStatus} actualValue={statusCode} changeValue={setStatusCode}/>
                </div>
                       
                <p className="alert"> <strong>0:</strong> Planejamento <strong>1:</strong> Inscrição <strong>2:</strong> Em Andamento <strong>3:</strong> Concluído </p>
               
                <div className="create-form-button">
                    <Botao fillColor="blue" label="Criar Período" clickFunction={() => { postSeasonReq( config, seasonYear, season, statusCode, setSeasonYear, setSeason, setStatusCode )}}/>
                </div>

            </div>
            <Footer />
        </div>
        </section>    
    )
}

export default SeasonEdit;
