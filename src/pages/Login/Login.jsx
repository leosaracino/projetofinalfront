import React from 'react';
import './Login.css';
import Form from './Form/Form';

function Login() {
    return (
        <>
        <div className="wrapper">
            <div className="login-container">
                <div className="main-info">
                    <h1>UFF</h1>
                    <h2>Bem vindo ao Sistema Acadêmico da Graduação</h2>
                    <p>Nosso novo design foi desenvolvido com o propósito de servir aos alunos de maneira mais moderna, simples e efetiva.
                    Contando com um design extremamente limpo e um servidor bem menos congestionado.
                    </p>
                </div>
                <div className="login-area">
                    <Form />
                </div>
            </div>
        </div>
        </>
    )
}

export default Login;
