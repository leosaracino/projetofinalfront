import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Context } from '../../../Context/AuthContext';


import Botao from '../../../common/Botao/Botao';
import Input from '../../../common/Input/Input';
import './Form.css';

function Form() {
    const [login, setLogin] = useState("11122233344");
    const [password, setPassword] = useState("123456");

    const history = useHistory();
    const { handleLogin } = useContext(Context);

    return (
        <>
        <h2>Acesse o seu idUFF</h2>
        <div className="form">   
            <Input type="number" label="Seu CPF (somente números)" placeholder="000.000.000-00" actualValue={login} changeValue={setLogin}/>
            <Input type="password" label="Senha do seu idUFF" placeholder="coxinha123"  actualValue={password} changeValue={setPassword}/>
            <Botao label="Logar" fillColor="blue" clickFunction={() => handleLogin(history, login, password)}/>
        </div>
        </>
    )
}

export default Form;
