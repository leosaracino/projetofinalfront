import React, { useContext, useState } from 'react';
import { Context } from '../../../Context/AuthContext';
import { useHistory } from 'react-router-dom';

import Input from '../../../common/Input/Input';
import Botao from '../../../common/Botao/Botao';

import './EditForm.css';

function EditForm() {
    const [name, setName] = useState("");
    const [street, setStreet] = useState("");
    const [number, setNumber] = useState("");
    const [neighborhood, setNeighborhood] = useState("");
    const [complement, setComplement] = useState("");
    const [state, setState] = useState("");
    const [cep, setCep] = useState("");
    const [tel, setTel] = useState("");
    const [cel, setCel] = useState("");

    const [password, setPassword] = useState("");
    const [passwordConfirmation, setPasswordConfirmation] = useState("");

    const history = useHistory(); 
    const userData = JSON.parse(localStorage.getItem('userLogged'));

    const { config, setNavigation, handleEdition } = useContext(Context);
    
    return (
        <>
        <div className="edit-wrapper">
            <h3>Suas Informações</h3>
            <div className="upper-edit-section">
                <Input type="text" label="Nome Completo" placeholder="Mairinho, Amandinha e Leozinho :)" actualValue={name} changeValue={setName}/>
                <p>{`Nacionalidade: ${userData.nationality}`}</p>
                <p>{`Estado: ${userData.state}`}</p>
                <div className="key-infos">
                    <p><strong>RG: </strong>{userData.rg}</p>
                    <p><strong>CPF: </strong>{userData.cpf}</p>
                </div>
            </div>

            <h3>Dados para contato</h3>

            <div className="lower-edit-section">
                <div className="contact-info">
                    <Input type="text" label="Rua" placeholder="Rua dos bobos" id="street" actualValue={street} changeValue={setStreet}/>
                    <Input type="text" label="Nº" placeholder="2345meia78" id="number" actualValue={number} changeValue={setNumber}/>
                    <Input type="text" label="Bairro" placeholder="Ta na hora de molhar o biscoito" id="neighborhood" actualValue={neighborhood} changeValue={setNeighborhood }/>
                    <Input type="text" label="Complemento" placeholder="AAAAAAAAAAA" id="complement" actualValue={complement} changeValue={setComplement}/>
                    <Input type="text" label="Estado" placeholder="??????????" id="state" actualValue={state} changeValue={setState}/>
                    <Input type="text" label="CEP" placeholder="00000-000" id="cep" actualValue={cep} changeValue={setCep}/>
                    <Input type="text" label="Telefone" placeholder="(21) 99999-9999" id="tel" actualValue={tel} changeValue={setTel}/>
                    <Input type="text" label="Celular" placeholder="(21) 9999-9999" id="cel" actualValue={cel} changeValue={setCel}/>               
                </div>

                <div className="password-info">
                    <Input type="password" label="Nova senha" placeholder="coxinha123" id="old-password" actualValue={password} changeValue={setPassword}/>
                    <Input type="password" label="Confirmação de senha" placeholder="coxinha123" id="new-password" actualValue={passwordConfirmation} changeValue={setPasswordConfirmation}/>
                </div>
                
                <p className="alert"> Atenção! Caso você selecione a opção "Salvar", você precisará ser desconectado para que as mudanças sejam atualizadas</p>
                <div className="action-buttons">
                    <Botao fillColor="red" label="Cancelar" clickFunction={() => setNavigation(history, 'my-dashboard')}/>
                    <Botao fillColor="blue" label="Salvar" clickFunction={() => handleEdition(config, history, state, userData.id)}/>
                </div>
            </div>
        </div>
        </>
    )
}

export default EditForm;
