import React from 'react';
import PageHeader from '../../common/PageHeader/PageHeader';
import SideBar from '../../common/SideBar/SideBar';
import './EditProfile.css';
import EditForm from './EditForm/EditForm';
import Footer from '../../common/Footer/Footer';

function EditProfile() {

    return (
        <section>
        <SideBar />
        <div className="page-wrapper">
            <div className="page-container">
                <PageHeader title="Editar Perfil"/>
                <p>Edite os dados cadastrados para sua conta do IDUFF</p>
                <EditForm />     
            </div>
        <Footer />
        </div>
        </section>        
    )
}

export default EditProfile;
