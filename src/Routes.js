import React, { useContext } from 'react';
import { BrowserRouter as Router, Switch, Redirect, Route } from 'react-router-dom';

import { Context } from './Context/AuthContext';

import Login from './pages/Login/Login';
import Resume from './pages/Resume/Resume';
import NotFound from './pages/NotFound/NotFound';
import Forbidden from './pages/Forbidden/Forbidden';
import EditProfile from './pages/EditProfile/EditProfile';
import SeasonEdit from './pages/Private/PrincipalPages/SeasonEdit';
import CoordRegistration from './pages/Private/PrincipalPages/CoordRegistration';
import CourseRegistration from './pages/Private/PrincipalPages/CourseRegistration';
import DepartmentRegistration from './pages/Private/PrincipalPages/DepartmentRegistration';
import StudentRegistration from './pages/Private/CoordCPages/StudentRegistration';
import CourseVisualization from './pages/Private/CoordCPages/CourseVisualization';
import StudentsVisualization from './pages/Private/CoordCPages/StudentsVisualization';

function CustomRoute({ role, isPrivate, ...rest }) {
    const userData = JSON.parse(localStorage.getItem('userLogged'));
    const { loading , authenticated } = useContext(Context);

    if (loading) {
        return <h3>Loading...</h3>;
    }

    if ((isPrivate && !authenticated) || (isPrivate && role !== userData.role && typeof role !== "undefined")) {
        return <Redirect to='/401' />
    }

    return <Route {...rest}/>;
}

function Routes() {
    return (
        <Router>
            <Switch>
                {/* ------------- COMMON ROUTES ---------------*/}
                <CustomRoute exact path="/" component={Login}/>
                <CustomRoute exact path="/401" component={Forbidden}/>
                <CustomRoute exact path="/404" component={NotFound}/>

                {/* -------------------------- ALL USERS COMMON ROUTES ----------------------------*/}
                <CustomRoute isPrivate exact path="/my-dashboard" component={Resume}/>
                <CustomRoute isPrivate exact path="/my-dashboard/profile-edit" component={EditProfile}/>

                {/* -------------------------- PRINCIPAL PRIVATE ROUTES -------------------------- */}
                <CustomRoute isPrivate exact path="/my-dashboard/coord-register" component={CoordRegistration} role="director"/>
                <CustomRoute isPrivate exact path="/my-dashboard/season-edit" component={SeasonEdit} role="director"/>
                <CustomRoute isPrivate exact path="/my-dashboard/course-register" component={CourseRegistration} role="director"/>
                <CustomRoute isPrivate exact path="/my-dashboard/department-register" component={DepartmentRegistration} role="director"/>

                {/* -------------------------- COURSE COORDINATOR PRIVATE ROUTES --------------------------------- */}
                <CustomRoute isPrivate exact path="/my-dashboard/course" component={CourseVisualization} role="coordC"/>
                <CustomRoute isPrivate exact path="/my-dashboard/students" component={StudentsVisualization} role="coordC"/>
                <CustomRoute isPrivate exact path="/my-dashboard/student-register" component={StudentRegistration} role="coordC"/>

                {/* ------------- REDIRECT -------------- */}
                <CustomRoute path ="/"><Redirect to="/404"/></CustomRoute>
            </Switch>
        </Router>      
    )
}

export default Routes;

