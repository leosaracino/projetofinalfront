import React from 'react';

import './SelectBox.css';

function SelectBox({ label , listedOptions, actualValue, changeValue }) {
    return (
        <select className="input-style" 
        value={actualValue} 
        onChange={(e) => {changeValue(e.target.value)}}
        >
            <option 
            value="" defaultValue>
                {label}
            </option>

            {listedOptions.map( i => {
                return (
                    <option key={i} value={i}>{i}</option>
                )
            })}
        </select>
    )
}

export default SelectBox;
