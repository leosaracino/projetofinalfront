import React from 'react';
import './Botao.css'
// import {Link} from 'react-router-dom';

function Botao({ clickFunction, label, fillColor}) {
    return (
        <button onClick={clickFunction} 
            className={`botao ${fillColor}`}>
                {label}
        </button>
    )
}

export default Botao;
