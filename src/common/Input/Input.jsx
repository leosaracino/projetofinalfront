import React from 'react';
import "./Input.css";

function Input({type, placeholder, label, actualValue, changeValue, id}) {
    return (
        <div>
        <label htmlFor={id}>{label}</label>
        <input className="input-style" 
            value={actualValue}
            onChange={(e) => {changeValue(e.target.value)}} 
            type={type} 
            placeholder={placeholder}
            id={id}
            required
        />
        </div>
    )
}

export default Input;
