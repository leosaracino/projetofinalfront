import React from 'react';
import './PageHeader.css';

function PageHeader({title}) {
    return (
        <h1 className="page-title">{title}</h1>
    )
}

export default PageHeader;
