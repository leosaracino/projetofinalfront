import React, { useContext }from 'react';
import { Context } from '../../../Context/AuthContext';
import { useHistory } from 'react-router-dom';

import Botao from '../../Botao/Botao';
import './Profile.css';



function Profile() {    
    const history = useHistory();

    const userData = JSON.parse(localStorage.getItem('userLogged'));

    const { setNavigation } = useContext(Context);

    return (
        <>
        <div className="profile-card">
            <div className="element-info">
                <p>{userData.name}</p>
                <p>{userData.role}</p>
                <p>{userData.email}</p>
            </div>
            <Botao fillColor="blue" label="Editar dados" clickFunction={() => setNavigation(history, `my-dashboard/profile-edit`)}/>
        </div>
        </>
    )
}

export default Profile;
