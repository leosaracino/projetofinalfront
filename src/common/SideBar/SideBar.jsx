import React , { useContext } from 'react';
import { Context } from '../../Context/AuthContext';
import { useHistory } from 'react-router-dom';

import Profile from './Profile/Profile';
import Menu from './Menu/Menu';
import Botao from '../Botao/Botao';

import './Sidebar.css';



function SideBar() {

    const history = useHistory();
    const { handleLogout } = useContext(Context);

    return (
        <>
        <div className="side-bar">
            <Profile />
            <Menu />
            <Botao fillColor="blue" label="Sair" clickFunction={() => handleLogout(history)} link="/"/>
        </div>
        </>
    )
}

export default SideBar;
