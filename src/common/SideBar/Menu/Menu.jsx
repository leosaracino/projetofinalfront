import React from 'react';
import Principal from './Components/Principal/Principal';
import CoordC from './Components/CoordC/CoordC';
import CoordD from './Components/CoordD/CoordD';
import Teacher from './Components/Teacher/Teacher';
import Student from './Components/Student/Student';
import './Menu.css';


function Menu() {
    const userData = JSON.parse(localStorage.getItem('userLogged'));

    function renderRole(data) {
        switch (data.role) {
            case 'director':
                return <Principal />       
          
            case 'coordC':
                return <CoordC />
                    
            case 'coordD':
                return <CoordD />
                
            case 'teacher':
                return <Teacher />
                
            case 'student':
                return <Student />

            default:
                break
        }
    }

    return (
        <nav className="nav-menu">
            {renderRole(userData)}
        </nav>
    )
}

export default Menu;
