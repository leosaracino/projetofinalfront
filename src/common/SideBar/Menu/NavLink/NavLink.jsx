import React from 'react';
import './NavLink.css'

function rightLink (titulo) {

    switch (titulo) {
        case "Ver Currículos":
            return "/my-dashboard/";

        case "Cadastrar Coordenador":
            return "/my-dashboard/coord-register";

        case "Cadastrar Cursos":
            return "/my-dashboard/course-register";

        case "Cadastrar Departamentos":
            return "/my-dashboard/department-register";

        case "Editar Períodos":
            return "/my-dashboard/season-edit";
        
        case "Ver Curso":
            return "/my-dashboard/course"
        
        case "Alunos":
            return "/my-dashboard/students"

        case "Cadastrar Aluno":
            return "/my-dashboard/student-register";
                            
        default:
            break;
    }
}

function NavLink({label}) {
    return (
        <a href={rightLink(label)} className="nav-link" >{label}</a>
    )
}

export default NavLink;
