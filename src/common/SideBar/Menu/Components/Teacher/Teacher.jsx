import React from 'react'
import NavLink from '../../NavLink/NavLink'

function Teacher() {
    return (
        <>
        <NavLink label="Ver Currículos"/>
        <NavLink label="Ver Turmas"/>
        </>
    )
}

export default Teacher
