import React from 'react';
import NavLink from '../../NavLink/NavLink'

function Student() {
    return (
        <>
        <NavLink label="Ver Currículos"/>
        <NavLink label="Ver Turmas"/>
        <NavLink label="Inscrição em disciplinas"/>
        <NavLink label="Histórico"/>
        </>
    )
}

export default Student;
