import React from 'react';
import NavLink from '../../NavLink/NavLink';

function CoordC() {
    return (
        <>
        <NavLink label="Ver Currículos"/>
        <NavLink label="Ver Curso"/>
        <NavLink label="Alunos"/>
        <NavLink label="Cadastrar Aluno"/>
        </>
    )
}

export default CoordC;
