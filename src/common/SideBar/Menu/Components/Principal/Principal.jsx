import React from 'react';
import NavLink from '../../NavLink/NavLink'

function Principal() {
    return (
        <>
        <NavLink label="Ver Currículos"/>
        <NavLink label="Cadastrar Coordenador"/>
        <NavLink label="Cadastrar Cursos"/>
        <NavLink label="Cadastrar Departamentos"/>
        <NavLink label="Editar Períodos"/>
        </>
    )
}

export default Principal;
