import React from 'react'
import NavLink from '../../NavLink/NavLink'

function CoordD() {
    return (
        <>
        <NavLink label="Ver Currículos"/>
        <NavLink label="Ver Período"/>
        <NavLink label="Ver Departamento"/>
        <NavLink label="Ver Professores"/>
        <NavLink label="Ver Disciplinas"/>
        <NavLink label="Ver Turmas"/>
        <NavLink label="Cadastrar Professor"/>
        <NavLink label="Cadastrar Disciplina"/>
        </>
    )
}

export default CoordD
