import React , { createContext } from 'react';
import axios from 'axios';

import useAuth from './Hooks/useAuth';

import api from '../service/Api'

const Context = createContext();

let config = {};
config.headers = {"Authorization": localStorage.getItem('token').replace(/"/g, "")};

function setNavigation(history, destination) {
    history.push(`/${destination}`)
}

function AuthProvider({children}) {

    const IBGE = axios.create({ baseURL: "https://servicodados.ibge.gov.br/api/v1/localidades/"});

    const { authenticated, loading, handleLogin, handleLogout, handleEdition } = useAuth();

    return (
        <Context.Provider value={ { loading, authenticated, handleLogin, handleLogout, setNavigation, handleEdition , api, IBGE, config } }>
            { children }
        </Context.Provider>
    )
}

export { Context, AuthProvider };
