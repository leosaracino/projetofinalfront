import api from '../../service/Api';

function postUserReq( config, charge, name, birth, rg, cpf, state, email, nationality, setCharge, setName, setState, setBirth, setPlate, setCpf, setEmail, setNacionality) {
    
    function cpfPattern(login) {
        login = login.replace(/[^\d]/g, "");
      
        return login.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
    }

    function birthdatePattern(date) {
        date = date.replace(/[^\d]/g, "");

        return date.replace(/(\d{2})(\d{2})(\d{4})/, "$1/$2/$3");
    }

    function rgPattern(document) {
        document = document.replace(/[^\d]/g, "");
        return document.replace(/(\d{2})(\d{3})(\d{3})(\d{1})/, "$1.$2.$3-$4");
    }

    function assignRole(userRole) {
        switch (userRole) {
            case 'Coordenador de departamento':
                return 'coordD';

            case 'Coordenador de curso':
                return 'coordC';

            default:
                break;
        }
    }

    const personCode = cpfPattern(cpf);
    const personBDay = birthdatePattern(birth);
    const personRG = rgPattern(rg);

    const role = assignRole(charge);

    let output = {
        "user": {
            name,
            email,
            "cpf": personCode,
            "rg": personRG,
            role,
            "birthdate": personBDay,
            state,
            nationality
        }
    }

    console.log(output)

    api.post('/users' , output, config)
    .then((response) => {
        setCharge("");
        setName("");
        setState("");
        setBirth("");
        setPlate("");
        setCpf("");
        setEmail("");
        setNacionality("");
        console.log(response.data);
        alert(`Novo ${charge} criado com sucesso!`);
    })
    .catch((err) => {
        setCharge("");
        setName("");
        setState("");
        setBirth("");
        setPlate("");
        setCpf("");
        setEmail("");
        setNacionality("");
        console.warn(err);
        alert("Tivemos um erro interno em nosso servidor :/");
    });
}

export { postUserReq };