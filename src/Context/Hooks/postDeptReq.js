import api from '../../service/Api';

function postDeptReq(config, knowledgeArea, campus, courseCode, courseName, userId, setKnowledgeArea, setCampus, setCourseCode, setCourseName, setUserId) {

    let output = {
        "department": {
            "code": courseCode,
            "campus": campus,
            "knowledge_area": knowledgeArea, 
            "name" : courseName,
            "period_id": 1,
            "user_id" : parseInt(userId)
        }
    }

    console.log(output);

    api.post('/departments' , output, config)
    .then((response) => {
        alert(`Departamento de ${courseName} criado com sucesso!`);
        setKnowledgeArea("");
        setCampus("");
        setCourseCode("");
        setCourseName("");
        setUserId("");
        console.log(response.data)
    })
    .catch((err) => {
        alert("Tivemos um erro interno em nosso servidor :/");
        console.warn(err);
    });
}

export { postDeptReq };