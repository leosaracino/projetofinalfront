import api from '../../service/Api';

function postCourseReq(config, knowledgeArea, campus, courseCode, courseName, userId, setKnowledgeArea, setCampus, setCourseCode, setCourseName, setUserId) {

    let output = {
        "course": {
            "code": courseCode,
            "campus": campus,
            "knowledge_area": knowledgeArea, 
            "name" : courseName,
            "number_students": 0,
            "period_id": 1,
            "user_id" : parseInt(userId)
        }
    }

    console.log(output);

    api.post('/courses' , output, config)
    .then((response) => {
        alert(`Curso de ${courseName} criado com sucesso!`);
        setKnowledgeArea("");
        setCampus("");
        setCourseCode("");
        setCourseName("");
        setUserId("");
        console.log(response.data)
    })
    .catch((err) => {
        alert("Tivemos um erro interno em nosso servidor :/");
        console.warn(err);
    });
}

export { postCourseReq };