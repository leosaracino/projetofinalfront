import { useState , useEffect } from 'react';

import api from '../../service/Api';

export default function useAuth() {
    const [ authenticated, setAuthenticated ] = useState(false);
    const [ loading , setLoading ] = useState(true);

    useEffect( () => {
        const token = localStorage.getItem('token');
        const userLogged = localStorage.getItem('userLogged');
        
        if (token) {
            api.defaults.headers.Authorization = `Bearer ${JSON.parse(token)}`;
            setAuthenticated(true);            
        }

        if(userLogged) {
            api.defaults.headers.Authorization = userLogged;
        }

        setLoading(false);
    }, []);

    function loginPattern(login) {
        login = login.replace(/[^\d]/g, "");
      
        return login.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
    }

    const handleLogin = (history, login, password) => {

        const cpf = loginPattern(login);

        api.post('/login', {
            user: {
                cpf,
                password
            }
        })
        .then((response) => {
            localStorage.setItem('token', JSON.stringify(response.data.token));
            localStorage.setItem('userLogged', JSON.stringify(response.data.user));
            api.defaults.headers.Authorization = response.data.user;
            api.defaults.headers.Authorization = `Bearer ${response.data.token}`;
            setAuthenticated(true);
            history.push('/my-dashboard');
        })
        .catch((err) => {
            console.log(err);
            alert(err.response.data.message);
        });
    }

    function handleLogout(history) {    
        localStorage.removeItem('token');
        localStorage.removeItem('userLogged');
        api.defaults.headers.Authorization = undefined;
        setAuthenticated(false);
        history.push('/');
    }

    function handleEdition(config, history, lista, userId) {

        let output = {};

        let keys = [ 
            "name",
            "street",
            "number",
            "neighborhood",
            "complement",
            "cep",
            "telephone",
            "cellphone",
            "password",
            "password_confirmation"
        ]

        for (let x in keys) {
            output[keys[x]] = lista[x]
        }
        
        api.put(`/users/${userId}`, {user:{output}}, config)
        .then(() => {
            handleLogout(history);
        })
        .catch((err) => {
            console.warn(err);
            alert(err.response.data.message);
        })
    }

    return { authenticated, loading, handleLogin, handleLogout, handleEdition };
}