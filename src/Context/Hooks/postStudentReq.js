import api from '../../service/Api';

function postStudentReq(config, name, birth, rg, cpf, state, email, nationality, setName, setBirth, setRg, setCpf, setState, setEmail, setNacionality) {
    
    function cpfPattern(login) {
        login = login.replace(/[^\d]/g, "");
      
        return login.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
    }

    function birthdatePattern(date) {
        date = date.replace(/[^\d]/g, "");

        return date.replace(/(\d{2})(\d{2})(\d{4})/, "$1/$2/$3");
    }

    function rgPattern(document) {
        document = document.replace(/[^\d]/g, "");
        return document.replace(/(\d{2})(\d{3})(\d{3})(\d{1})/, "$1.$2.$3-$4");
    }

    const personCode = cpfPattern(cpf);
    const personBDay = birthdatePattern(birth);
    const personRG = rgPattern(rg);

    let output = {
        "user": {
            name,
            email,
            "role": "student",
            "cpf": personCode,
            "rg": personRG,
            "birthdate": personBDay,
            state,
            nationality
        }
    }

    console.log(output)

    api.post('/create_student' , output, config)
    .then((response) => {
        setName("");
        setState("");
        setBirth("");
        setCpf("");
        setEmail("");
        setRg("");
        setNacionality("");
        console.log(response.data);
        alert(`Usuário do aluno ${name} criado com sucesso!`);
    })
    .catch((err) => {
        setName("");
        setState("");
        setBirth("");
        setCpf("");
        setEmail("");
        setRg("");
        setNacionality("");
        console.warn(err);
        alert("Tivemos um erro interno em nosso servidor :/");
    });
}

export { postStudentReq };