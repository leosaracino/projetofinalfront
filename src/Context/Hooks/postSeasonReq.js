import api from '../../service/Api';

function postSeasonReq(config, year, season, status, setSeasonYear, setSeason, setStatusCode) {

    let output = {
        "period":{
            "year": year, 
            "semester": season, 
            "status": parseInt(status)
        }
    }

    console.log(output)
    api.post('/periods' , output, config)
    .then((response) => {
        alert("Período criado com sucesso!");
        setSeasonYear("");
        setSeason("");
        setStatusCode("");
        console.log(response.data)
    })
    .catch((err) => {
        alert("Tivemos um erro interno em nosso servidor :/");
        console.warn(err);
    });
}

export { postSeasonReq };
